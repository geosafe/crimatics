type = ['','info','success','warning','danger'];

map_functions={ 
  initOpenLayers:function(){
    var blur = document.getElementById('blur');
    var radius = document.getElementById('radius');

    var raster = new ol.layer.Tile({
			source: new ol.source.OSM()
		});

		var source = new ol.source.Vector({wrapX: false});

		var vector = new ol.layer.Vector({
			source: source
		});

		var heatVect = new ol.layer.Heatmap({
			source: new ol.source.Vector({
				url: 'https://openlayers.org/en/v4.2.0/examples/data/kml/2012_Earthquakes_Mag5.kml',
				format: new ol.format.KML({
					extractStyles: false
				})
			}),
			blur: parseInt(blur.value, 10),
			radius: parseInt(radius.value, 10)
		});

		var map = new ol.Map({
			controls: [],
			layers: [raster, vector, heatVect],
			target: 'map',
			view: new ol.View({
				extent: [-10000000, 8000000, -10000000, 5000000],
				center: [-11000000, 4600000],
				zoom: 4.25,
				minZoom: 4.25
			})
		});

  },
  initGoogleMaps: function(){
        var myLatlng = new google.maps.LatLng(40.748817, -95.985428);
        var mapOptions = {
          zoom: 5,
          center: myLatlng,
          scrollwheel: true, //we disable de scroll over the map, it is a really annoing when you scroll through page
          styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]

        }
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
    
        // Open Layers
        var view = new ol.View({
      // make sure the view doesn't go beyond the 22 zoom levels of Google Maps
      maxZoom: 21
    });
    view.on('change:center', function() {
      var center = ol.proj.transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');
      gmap.setCenter(new google.maps.LatLng(center[1], center[0]));
    });
    view.on('change:resolution', function() {
      gmap.setZoom(view.getZoom());
    });

    var vector = new ol.layer.Vector({
      source: new ol.source.GeoJSON({
        url: 'data/geojson/countries.geojson',
        projection:  'EPSG:3857'
      }),
      style: new ol.style.Style({
        fill: new ol.style.Fill({
          color: 'rgba(255, 255, 255, 0.9)'
        }),
        stroke: new ol.style.Stroke({
          color: '#319FD3',
          width: 1
        })
      })
    });

    var olMapDiv = document.getElementById('olmap');
    var map = new ol.Map({
      layers: [vector],
      interactions: ol.interaction.defaults({
        altShiftDragRotate: false,
        dragPan: false,
        rotate: false
      }).extend([new ol.interaction.DragPan({kinetic: null})]),
      target: olMapDiv,
      view: view
    });
    view.setCenter([0, 0]);
    view.setZoom(1);

    },
  
  
  initGoogleLayers:function(){
    var gmap = new google.maps.Map(document.getElementById("map"), {
      disableDefaultUI: true,
      keyboardShortcuts: false,
      draggable: false,
      disableDoubleClickZoom: true,
      scrollwheel: false,
      streetViewControl: false
    });

    var view = new ol.View({
      // make sure the view doesn't go beyond the 22 zoom levels of Google Maps
      maxZoom: 21
    });
    view.on('change:center', function() {
      var center = ol.proj.transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');
      gmap.setCenter(new google.maps.LatLng(center[1], center[0]));
    });
    view.on('change:resolution', function() {
      gmap.setZoom(view.getZoom());
    });

    var vector = new ol.layer.Vector({
      source: new ol.source.GeoJSON({
        url: 'data/geojson/countries.geojson',
        projection:  'EPSG:3857'
      }),
      style: new ol.style.Style({
        fill: new ol.style.Fill({
          color: 'rgba(255, 255, 255, 0.6)'
        }),
        stroke: new ol.style.Stroke({
          color: '#319FD3',
          width: 1
        })
      })
    });

    var olMapDiv = document.getElementById('olmap');
    var map = new ol.Map({
      layers: [vector],
      interactions: ol.interaction.defaults({
        altShiftDragRotate: false,
        dragPan: false,
        rotate: false
      }).extend([new ol.interaction.DragPan({kinetic: null})]),
      target: olMapDiv,
      view: view
    });
    view.setCenter([0, 0]);
    view.setZoom(1);
  }
}



//demo = {
//    initPickColor: function(){
//        $('.pick-class-label').click(function(){
//            var new_class = $(this).attr('new-class');
//            var old_class = $('#display-buttons').attr('data-class');
//            var display_div = $('#display-buttons');
//            if(display_div.length) {
//            var display_buttons = display_div.find('.btn');
//            display_buttons.removeClass(old_class);
//            display_buttons.addClass(new_class);
//            display_div.attr('data-class', new_class);
//            }
//        });
//    },
//
//    initFormExtendedDatetimepickers: function(){
//        $('.datetimepicker').datetimepicker({
//            icons: {
//                time: "fa fa-clock-o",
//                date: "fa fa-calendar",
//                up: "fa fa-chevron-up",
//                down: "fa fa-chevron-down",
//                previous: 'fa fa-chevron-left',
//                next: 'fa fa-chevron-right',
//                today: 'fa fa-screenshot',
//                clear: 'fa fa-trash',
//                close: 'fa fa-remove'
//            }
//         });
//    },
//
//    initDocumentationCharts: function(){
//        /* ----------==========     Daily Sales Chart initialization For Documentation    ==========---------- */
//
//        dataDailySalesChart = {
//            labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
//            series: [
//                [12, 17, 7, 17, 23, 18, 38]
//            ]
//        };
//
//        optionsDailySalesChart = {
//            lineSmooth: Chartist.Interpolation.cardinal({
//                tension: 0
//            }),
//            low: 0,
//            high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
//            chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
//        }
//
//        var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);
//
//        md.startAnimationForLineChart(dailySalesChart);
//    },
//
//    initDashboardPageCharts: function(){
//
//        /* ----------==========     Daily Sales Chart initialization    ==========---------- */
//
//        dataDailySalesChart = {
//            labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
//            series: [
//                [12, 17, 7, 17, 23, 18, 38]
//            ]
//        };
//
//        optionsDailySalesChart = {
//            lineSmooth: Chartist.Interpolation.cardinal({
//                tension: 0
//            }),
//            low: 0,
//            high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
//            chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
//        }
//
//        var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);
//
//        md.startAnimationForLineChart(dailySalesChart);
//
//
//
//        /* ----------==========     Completed Tasks Chart initialization    ==========---------- */
//
//        dataCompletedTasksChart = {
//            labels: ['12am', '3pm', '6pm', '9pm', '12pm', '3am', '6am', '9am'],
//            series: [
//                [230, 750, 450, 300, 280, 240, 200, 190]
//            ]
//        };
//
//        optionsCompletedTasksChart = {
//            lineSmooth: Chartist.Interpolation.cardinal({
//                tension: 0
//            }),
//            low: 0,
//            high: 1000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
//            chartPadding: { top: 0, right: 0, bottom: 0, left: 0}
//        }
//
//        var completedTasksChart = new Chartist.Line('#completedTasksChart', dataCompletedTasksChart, optionsCompletedTasksChart);
//
//        // start animation for the Completed Tasks Chart - Line Chart
//        md.startAnimationForLineChart(completedTasksChart);
//
//
//
//        /* ----------==========     Emails Subscription Chart initialization    ==========---------- */
//
//        var dataEmailsSubscriptionChart = {
//          labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
//          series: [
//            [542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]
//
//          ]
//        };
//        var optionsEmailsSubscriptionChart = {
//            axisX: {
//                showGrid: false
//            },
//            low: 0,
//            high: 1000,
//            chartPadding: { top: 0, right: 5, bottom: 0, left: 0}
//        };
//        var responsiveOptions = [
//          ['screen and (max-width: 640px)', {
//            seriesBarDistance: 5,
//            axisX: {
//              labelInterpolationFnc: function (value) {
//                return value[0];
//              }
//            }
//          }]
//        ];
//        var emailsSubscriptionChart = Chartist.Bar('#emailsSubscriptionChart', dataEmailsSubscriptionChart, optionsEmailsSubscriptionChart, responsiveOptions);
//
//        //start animation for the Emails Subscription Chart
//        md.startAnimationForBarChart(emailsSubscriptionChart);
//
//    },
//
//    initGoogleMaps: function(){
//        var myLatlng = new google.maps.LatLng(40.748817, -95.985428);
//        var mapOptions = {
//          zoom: 5,
//          center: myLatlng,
//          scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
//          styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]
//
//        }
//        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
//
//        var marker = new google.maps.Marker({
//            position: myLatlng,
//            title:"Hello World!"
//        });
//
//        // To add the marker to the map, call setMap();
//        marker.setMap(map);
//    },
//
//	showNotification: function(from, align){
//    	color = Math.floor((Math.random() * 4) + 1);
//
//    	$.notify({
//        	icon: "notifications",
//        	message: "Welcome to <b>Material Dashboard</b> - a beautiful freebie for every web developer."
//
//        },{
//            type: type[color],
//            timer: 4000,
//            placement: {
//                from: from,
//                align: align
//            }
//        });
//	}
//
//
//
//}
